require("dotenv").config(); //initialize dotenv
const Discord = require("discord.js"); //import discord.js
const client = new Discord.Client(); //create new client

function between(min, max) {
    return Math.floor(Math.random() * (max + 1 - min) + min);
}

client.on("ready", () => {
    console.log(`Logged in as ${client.user.tag}!`);
});

client.on("message", async (msg) => {
    const regex = new RegExp(" |d|D");
    if (msg.content === "/roll alive") {
        await msg.channel.send("Johnny Five is alive!", {
            files: ["https://media0.giphy.com/media/o2ITDLRkP2oGk/giphy.gif"],
        });
        return;
    }
    if (
        msg.content === "/roll shutdown" &&
        msg.author.username === "DrewK2525"
    ) {
        await msg.channel.send("Goodbye cruel world :sob:", {
            files: [
                "https://o.aolcdn.com/hss/storage/midas/5565f2658555fcd848c7d8c58426113/202094128/m2735b.gif",
            ],
        });
        console.log("Shut down time!");
        client.destroy();
        return;
    }
    let msgParts = msg.content.split(regex);
    let response = "Please enter a valid die to roll: ex D2, D6, D20, D%";
    try {
        if (msgParts[0] === "/roll") {
            const firstPart = msgParts[1].toUpperCase();
            let numberOfDice = parseInt(firstPart);
            if (numberOfDice > 1000) {
                msg.reply("Don't break me please");
                return;
            }
            if (firstPart == "CHARACTER") {
                msg.reply(
                    `${getCharacterRoll("Strength")}\n${getCharacterRoll(
                        "Dexterity"
                    )}\n${getCharacterRoll("Constitution")}\n${getCharacterRoll(
                        "Intelligence"
                    )}\n${getCharacterRoll("Wisdom")}\n${getCharacterRoll(
                        "Charisma"
                    )}`
                );
                return;
            }
            if (firstPart === "HELP") {
                try {
                    msg.reply(
                        "You can roll any number die, or a percent die by sending the command: `/roll D*` where * is any positive integer or the `%` symbol\n\nYou can also roll any number of dice by including an integer before the `D*`.\n\n Examples: `/roll d6`, `/roll 4 D6`, `/roll 4d6`, `/roll 4 6` (rolls 4 d6)\n\nYou can also use the command `/roll character` to roll up a new character (roll 4 drop 1)"
                    );
                } catch {}
                return;
            }
            console.log(numberOfDice);
            if (isNaN(numberOfDice)) {
                numberOfDice = 1;
            }
            if (numberOfDice < 1) {
                msg.reply(
                    "Please enter a valid die or integer as your first argument\nSubmit the command: `/roll help` for help."
                );
                return;
            }
            const die = msgParts[2] ? msgParts[2] : msgParts[3] || firstPart;
            const dieValue = die.startsWith("D") ? die.slice(1) : die;
            if (parseInt(dieValue) > 1000) {
                msg.reply("Don't break me please");
                return;
            }
            if (
                (isNaN(parseInt(dieValue)) || parseInt(dieValue) <= 0) &&
                dieValue !== "%"
            ) {
                msg.reply(
                    "Please submit a valid die to roll!\nSubmit the command: `/roll help` for help."
                );
                return;
            }
            response = diceRoll(die, dieValue, numberOfDice, msg);
            if (response.length >= 2000) {
                msg.reply("Don't break me");
                return;
            }
            msg.reply(response).catch(() => {
                msg.reply("Don't break me");
            });
        }
    } catch (e) {
        console.error(e);
        try {
            msg.reply("Unknown command, try again");
        } catch {}
    }
});

const getCharacterRoll = (ability) => {
    const results = [];
    for (let i = 0; i < 4; i++) {
        results.push(between(1, 6));
    }
    let response = "";
    let total = 0;
    results
        .sort((a, b) => b - a)
        .forEach((result, i) => {
            if (i < 3) {
                total += result;
            }
            response += i == 3 ? ` [${result}] ` : `${result},`;
        });
    return `*${ability}*: ${total}\n${response}`;
};

const diceRoll = (die, dieValue, numberOfDice, msg) => {
    let response = `Rolled ${numberOfDice} D${dieValue} and got: `;
    const results = [];
    for (let i = 0; i < numberOfDice; i++) {
        const lowerLimit = isNaN(dieValue) ? 0 : 1;
        const upperLimit = isNaN(dieValue) ? 100 : parseInt(dieValue);
        const result = between(lowerLimit, upperLimit);
        response += `${result} ${i === numberOfDice - 1 ? "" : ", "}`;
        results.push(result);
    }
    response +=
        results.length === 1
            ? ""
            : ` For a total of: ${results.reduce((acc, result) => {
                  return acc + result;
              }, 0)}`;
    if (dieValue === "%") {
        response += "%";
    }
    return response;
};

//make sure this line is the last line
client.login(process.env.CLIENT_TOKEN);
